<?php

require_once('scripts/aldridge.class.php');

$aldridge = new Aldridge();


$aldridge->check_redirect_login();


?>
<div class="panel-body buttons-widget">
		<div class="row clearfix">
		<?php 

			   $things_object = $aldridge->gather_things();


				if( count($things_object) > 0 )
				{

						$colors   = array('partition-green','partition-blue','partition-red','partition-azure');

						$ctr=0;

						$i=0;

						$google_map_loc = array();


						foreach ($things_object as  $value){

				
							if(isset($value['loc']))
							{

								$google_map_loc[] = array('name'=>@$value['pole_name'],'geo_loc'=>@$value['loc'],'alarms'=>@$value['alarms']);
								
							}

							if(isset($value['alarms'])){

										$alarm_count = 0;
										foreach ($value['alarms'] as $alarm) {

											foreach ($alarm as $alarm_value) {

												if(isset($alarm_value['state']) && $alarm_value['state']==1){
													$alarm_count++;
												}	
											}

										}
											
		
							}

							$ctr++; 

							if($ctr%4==0) $ctr=0;
																
							?>
							<div class="col-md-6 col-lg-3 col-sm-6">
								<div class="panel panel-default panel-white core-box">
									<div class="panel-tools">
									
									</div>
									<div class="panel-body no-padding">
										<div class=" padding-20 text-center core-icon <?php echo $colors[$ctr];?>">
											<i class="fa  fa-database  fa-2x icon-big "></i>
										</div>
										<div class="core-content">
											<h3 class="title block no-margin padding-5"><?php echo @$value['name'];?></h3>
											<span class="subtitle"></span>
											<div class="table-responsive">
												<table class="table padding-20" style="font-size:10px">
												<tr>
													<td>ALARM</td>

													<td class="padding-5">
													<a href="#" data-toggle="tooltip" data-placement="top" title="View Alarm"  alt="alarm_<?php echo @$value['id']?>"  class="view-alert  padding-5 text-center text-white tooltips <?php echo @$colors[$ctr];?>" ><i class="glyphicon glyphicon-search"></i></a>
														<span class="label label-danger vertical-align-top "><?php echo $alarm_count;?></span>
													</td>
												</tr>
												<tr>
													<td>PROPERTY</td>

													<td class="padding-5">
													<a href="#" data-toggle="tooltip" data-placement="top" title="View Property" alt="thing_<?php echo @$value['id']?>" class="view-property padding-5 text-center text-white tooltips <?php echo @$colors[$ctr];?>" ><i class="glyphicon glyphicon-search"></i></a>
													<span class="label label-success vertical-align-top "><?php echo count(@$value['properties']);?></span>
													</td>
												</tr>
											</table>
											</div>											
											<div  style="display:none"  id="thing_<?php echo @$value['id']?>" >
													<table  class="table table-striped table-hover"  >

											<thead>
													<tr>
														<th>Name</th>
														<th>Value</th>
													</tr>

											</thead>
											<tbody>
											<?php
											 	if(isset($value['properties']) && count($value['properties']) > 0){

											 		foreach($value['properties'] as $propety_name=>$properties){
													?>
											 			<tr>
															<td><?php echo isset($propety_name) ? $propety_name:'n/a';?></td>
															<td><?php echo (isset($properties['value']) ? $properties['value']:'n/a');?></td>
														</tr>
											 		<?php
											 		}

											 	}
											 	else
											 	{

											 		?>
											 			<tr>
															<td colspan="2">No properties found.</td>
															
											 		<?php
											 	}
											?>
							
											</tbody>
											
											</table>
											</div>

											<div  style="display:none"  id="alarm_<?php echo @$value['id']?>" >
													<table  class="table table-striped table-hover"  >

											<thead>
													<tr>
														<th>Name</th>
														<th>Value</th>
													</tr>

											</thead>
											<tbody>
											<?php
											 	if(isset($value['alarms']) && count($value['alarms']) > 0){

											 		foreach($value['alarms'] as $alert_name=>$alert_info){
													?>
											 			<tr>
															<td><?php echo isset($alert_name) ? $alert_name:'n/a';?></td>
															<?php 

																if(count($alert_info) > 0){


																?>

															<td>

																	<table class="table">
																	<?php
																	 	foreach($alert_info as $key=>$alert_info){

																	 		foreach($alert_info as $key_name=>$details){
																	 	?>
																			<tr>
																				<td><?php echo $key_name.':'.$details;?></td>
																			</tr>
																	<?php
																		}
																	}
																	?>
																	</table>																					  		

															</td>				
															<?php 
																}
															?>	
														</tr>
											 		<?php
											 		}

											 	}
											 	else
											 	{

											 		?>
											 			<tr>
															<td colspan="2">No alarm(s) found.</td>
															
											 		<?php
											 	}
											?>
							
											</tbody>
											
											</table>
											</div>
											
										</div>
									</div>
							
								</div>
							</div>
							<?php
							
							$i++;							
						}
					?>
				
					<?php
				}

		?>
						</div>
						<?php


				if(count($google_map_loc) > 0)
				{

					?>
						<input type="hidden" id="json-location" value="<?php echo htmlspecialchars(json_encode($google_map_loc)) ;?>"/>
					<?php				
				}


						?>
					<div class="row">
						<div class="col-md-12"  id="google-map-1" style="height: 500px;"></div>						
					</div>

		</div>

								
						
<script type="text/javascript">
	
	
        $(function(){

            aldridge.Init();
            
        });
</script>