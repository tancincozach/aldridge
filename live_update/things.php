<?php

require_once('scripts/aldridge.class.php');

$aldridge = new Aldridge();


$aldridge->check_redirect_login();


?>
<div class="panel-body buttons-widget">
		<div class="row clearfix">
		<?php 

			   $things_object = $aldridge->gather_things();


				if( count($things_object) > 0 )
				{

						$colors   = array('partition-green','partition-blue','partition-red','partition-azure');

						$ctr=0;

						$i=0;

						$google_map_loc = array();


						foreach ($things_object as  $value){

				
							if(isset($value['loc']))
							{

								$google_map_loc[] = array('name'=>@$value['pole_name'],'geo_loc'=>@$value['loc'],'alarms'=>@$value['alarms']);
								
							}

							if(isset($value['alarms'])){

										$alarm_count = 0;
										foreach ($value['alarms'] as $alarm) {

											foreach ($alarm as $alarm_value) {

												if(isset($alarm_value['state']) && $alarm_value['state']==1){
													$alarm_count++;
												}	
											}

										}
											
		
							}

							$ctr++; 

							if($ctr%4==0) $ctr=0;
																
							?>
							<div class="col-md-6 col-lg-3 col-sm-6">
								<div class="panel panel-default panel-white core-box">
									<div class="panel-tools">
									
									</div>
									<div class="panel-body no-padding">
										<div class=" padding-20 text-center core-icon <?php echo $colors[$ctr];?>">
											<i class="fa  fa-database  fa-2x icon-big "></i>
										</div>
										<div class="core-content">
											<h3 class="title block no-margin padding-5"><?php echo @$value['name'];?></h3>
											<span class="subtitle"></span>
											<div class="table-responsive">
												<table class="table padding-20" style="font-size:10px">
												<tr>
													<td>ALARM</td>

													<td class="padding-5">
													<a href="#" data-toggle="tooltip" data-placement="top" title="View Alarm"  alt="alarm_<?php echo @$value['id']?>"  class="view-alert  padding-5 text-center text-white tooltips <?php echo @$colors[$ctr];?>" ><i class="glyphicon glyphicon-search"></i></a>
														<span class="label label-danger vertical-align-top "><?php echo $alarm_count;?></span>
													</td>
												</tr>
												<tr>
													<td>PROPERTY</td>

													<td class="padding-5">
													<a href="#" data-toggle="tooltip" data-placement="top" title="View Property" alt="thing_<?php echo @$value['id']?>" class="view-property padding-5 text-center text-white tooltips <?php echo @$colors[$ctr];?>" ><i class="glyphicon glyphicon-search"></i></a>
													<span class="label label-success vertical-align-top "><?php echo count(@$value['properties']);?></span>
													</td>
												</tr>
											</table>
											</div>											
											<div  style="display:none"  id="thing_<?php echo @$value['id']?>" >
													<table  class="table table-striped table-hover"  >

											<thead>
													<tr>
														<th>Name</th>
														<th>Value</th>
													</tr>

											</thead>
											<tbody>
											<?php
											 	if(isset($value['properties']) && count($value['properties']) > 0){

											 		foreach($value['properties'] as $propety_name=>$properties){
													?>
											 			<tr>
															<td><?php echo isset($propety_name) ? $propety_name:'n/a';?></td>
															<td><?php echo (isset($properties['value']) ? $properties['value']:'n/a');?></td>
														</tr>
											 		<?php
											 		}

											 	}
											 	else
											 	{

											 		?>
											 			<tr>
															<td colspan="2">No properties found.</td>
															
											 		<?php
											 	}
											?>
							
											</tbody>
											
											</table>
											</div>

											<div  style="display:none"  id="alarm_<?php echo @$value['id']?>" >
													<table  class="table table-striped table-hover"  >

											<thead>
													<tr>
														<th>Name</th>
														<th>Value</th>
													</tr>

											</thead>
											<tbody>
											<?php
											 	if(isset($value['alarms']) && count($value['alarms']) > 0){

											 		foreach($value['alarms'] as $alert_name=>$alert_info){
													?>
											 			<tr>
															<td><?php echo isset($alert_name) ? $alert_name:'n/a';?></td>
															<?php 

																if(count($alert_info) > 0){


																?>

															<td>

																	<table class="table">
																	<?php
																	 	foreach($alert_info as $key=>$alert_info){

																	 		foreach($alert_info as $key_name=>$details){
																	 	?>
																			<tr>
																				<td><?php echo $key_name.':'.$details;?></td>
																			</tr>
																	<?php
																		}
																	}
																	?>
																	</table>																					  		

															</td>				
															<?php 
																}
															?>	
														</tr>
											 		<?php
											 		}

											 	}
											 	else
											 	{

											 		?>
											 			<tr>
															<td colspan="2">No alarm(s) found.</td>
															
											 		<?php
											 	}
											?>
							
											</tbody>
											
											</table>
											</div>
											
										</div>
									</div>
							
								</div>
							</div>
							<?php
							
							$i++;							
						}
					?>
				
					<?php
				}

		?>
						</div>
						<?php


				if(count($google_map_loc) > 0)
				{

					?>
						<input type="hidden" id="json-location" value="<?php echo htmlspecialchars(json_encode($google_map_loc)) ;?>"/>
					<?php				
				}


						?>
					<div class="row">
						<div class="col-md-12"  id="google-map-1" style="height: 500px;"></div>						
					</div>

		</div>

								
						

			<script type="text/javascript">

 var google_map_api =
 {


       setMap:function (m) {
                  x = m.getZoom();
                  c = m.getCenter();
                  google.maps.event.trigger(m, 'resize');
                  m.setZoom(x);
                  m.setCenter(c);
              }
          ,
        getAddress: function (latLng) {
                    $('#geo_address').remove();
                    geocoder = new google.maps.Geocoder();  
                    geocoder.geocode( {'latLng': latLng},
                      function(results, status) {
                 
                        if(status == google.maps.GeocoderStatus.OK) {
                          if(results[0]) {                          
                                                        
                             $("<input type=\"hidden\"  id=\"geo_address\" value=\""+results[0].formatted_address +"\" >").appendTo('body');
                          }
                          else {                           
                              $("<input type=\"hidden\"  id=\"geo_address\" value=\"No Result\" >").appendTo('body');
                          }
                        }
                        else {
                          $("<input type=\"hidden\"  id=\"geo_address\" value=\""+status+"\" >").appendTo('body');
                        }

                      });
            
               }
            ,
            initGoogleMap:function( map_container){     
              
              $('#location').remove();

              try
              {




   if($('#json-location').length > 0){

        	 google_location_data = JSON.parse($('#json-location').val());

	 if(google_location_data=='' || $.isEmptyObject(google_location_data)) throw "ERROR: Empty Coordinates for Google Map Api";
        	 
								var map = new google.maps.Map(map_container, {
								      zoom: 3,
								      center: new google.maps.LatLng(google_location_data[0].geo_loc.lat, google_location_data[0].geo_loc.lng),
								      mapTypeId: google.maps.MapTypeId.ROADMAP
								    });

								    var infowindow = new google.maps.InfoWindow();




		                  if(google_location_data!='')
		                	{

		                		var marker;
		                		$.each(google_location_data,function(i,val){

										var full_address =	'';

											full_address+='<table class="table table-bordered table-condensed danger">';
											full_address+='<thead>';
											full_address+='<tr class="info"><th class="text-center">'+val.name+'</th></tr>';
											full_address+='</thead>';

										if(!$.isEmptyObject(val.alarms)){

										
												full_address+='<tr class="danger">';
												full_address+='<td >ALARMS</td>';
												full_address+='</tr>';
											$.each(val.alarms,function(alarm_name,alarm_values){

												full_address+='<tr >';
												full_address+='<td><label>&nbsp;&nbsp;-'+alarm_name+'</label></td>';
												full_address+='</tr>';

											});
											full_address+='</table>';

										}

										full_address+='<table class="table table-bordered">';
										full_address+='<thead>';
										full_address+='<tr class="success"><th >ADDRESS</th></tr>';
										full_address+='</thead>';

		                				 full_address +='<tr><td><label>&nbsp;&nbsp;-'+val.geo_loc.addr.streetNumber +' '+
																val.geo_loc.addr.street +' '+ 
																val.geo_loc.addr.city  +' '+
																val.geo_loc.addr.state;+'</label></td></tr>';

		                				 full_address+='</table>';

								



									if(!$.isEmptyObject(val.alarms)){


										$.each(val.alarms,function(alarm_name,alarm_values){

												console.log(alarm_values);				
												console.log(alarm_values[0].state);

													if(alarm_values[0].state==1){
															marker = new google.maps.Marker({
													        position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
													          icon:new google.maps.MarkerImage("assets/images/exclamation mark yellow.png",
													                      new google.maps.Size(32, 32),
													                          new google.maps.Point(0,0),
													                          new google.maps.Point(16, 16)),
													        map: map
													      });
													}

													else{
/*
														 marker = new google.maps.Marker({
															        position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
															          icon:new google.maps.MarkerImage("assets/images/target-marker.png",
															                      new google.maps.Size(32, 32),
															                          new google.maps.Point(0,0),
															                          new google.maps.Point(16, 16)),
															        map: map
															      });*/


													}	
											});

															  
									}else{

											/*		 marker = new google.maps.Marker({
															        position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
															          icon:new google.maps.MarkerImage("assets/images/target-marker.png",
															                      new google.maps.Size(32, 32),
															                          new google.maps.Point(0,0),
															                          new google.maps.Point(16, 16)),
															        map: map
															      });
*/
									}
								


									      google.maps.event.addListener(marker, 'click', (function(marker, i) {
									        return function() {
													infowindow.setContent(full_address);
													infowindow.open(map, marker);
									        }
									      })(marker, i));
								    
			                	});	
		                	}
	
               
                  this.setMap(map);  


                	
                	
	
        }


                             
              }
              catch( err )
              {
                     alert(err.message||err);
              }
                 
 
            }            
}
				
				$(function(){
		

				google_map_api.initGoogleMap(document.getElementById("google-map-1"));		
			

					$('.view-alert, .view-property').click(function(event){

								var table_id = $(this).attr('alt');

								var $table = $('#'+table_id).clone();


								bootbox.alert($table.html());

								event.preventDefault();

					});

					
					  interval = setInterval(

                            function()
                            {

                        			ajaxLoaderWithOutOverlay('things.php', $("#ajax-content"));

                            }   
                                                               
                          ,60000);  

						
				});
				
			</script>