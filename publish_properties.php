<?php

session_start();

require_once('scripts/aldridge.class.php');

$aldridge = new Aldridge();


$aldridge->check_redirect_login();

$things_object = $aldridge->things;


?>
<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
									<li class="active">
										Publish Properties
									</li>
								</ol>
							</div>
						</div>

							<div class="col-sm-12">
								<!-- start: TEXT FIELDS PANEL -->
								<div class="panel panel-white">
									<div class="panel-heading">
										<h4 class="panel-title">Add Properties</span></h4>
										<div class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
													</li>
													<li>
														<a class="panel-refresh" href="#">
															<i class="fa fa-refresh"></i> <span>Refresh</span>
														</a>
													</li>
													<li>
													<li>
														<a class="panel-expand" href="#">
															<i class="fa fa-expand"></i> <span>Fullscreen</span>
														</a>
													</li>
												</ul>
											</div>
											<a class="btn btn-xs btn-link panel-close" href="#">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="panel-body">
										<form role="form"  method="post" name="property-frm"  class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-sm-2">
													Select Thing <span class="symbol required"></span>
												</label>
												<div class="col-sm-6">
													<select class="form-control" id="dropdown" name="thing">
														<option value="">Select...</option>
														<?php

														   	$things_object = $aldridge->things;
												   			foreach ($things_object as  $value){
														?>
															<option value="<?php echo @$value['name'];?>" style="text-transform:uppercase"><?php echo @$value['name'];?></option>
														<?php }?>
														
													</select>
												</div>
											</div>
											<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-1">
													<button class="btn btn-primary" id="add-property">Add Field</button>
													<button class="btn btn-warning" id="clear-property">Clear Field</button>
											</label>
												
											</div>
											<div id="property_fields">
												
											</div>	
											<label class="col-sm-11 control-label " for="form-field-1">
													<button class="btn btn-success pull-right" id="save-property">Submit</button>
											</label>																				
										</form>
									</div>
								</div>
								<!-- end: TEXT FIELDS PANEL -->
							</div>

						<script type="text/javascript">


						var Property = {


							publish:function( posted_data){

								$.post('scripts/ajax.php?controller=publish',posted_data,function( jsonOutput){

										console.log(jsonOutput);

								},'json');
							}


						}
							$(function(){

								$('#save-property').hide();

								$('#add-property').click(function(event){

							   			var $field_prop = $('<div class="form-group">'+
															'<label class="col-sm-2 control-label" for="form-field-1">'+
																'<input placeholder="Property Name" name="property_name[]"  class="form-control" type="text">'+
															'</label>'+
															'<div class="col-sm-9">'+
																'<input placeholder="Property Value" name="property_value[]" class="form-control" type="text">'+
															'</div>'+
														'</div>');

												$field_prop.appendTo($('#property_fields'));

												$('#save-property').show();

												event.preventDefault();
								});

								$('#clear-property').click(function(event){

								   $('#property_fields').html('');
								   $('#save-property').hide();
									event.preventDefault();
								});

								$('#save-property').click(function(event){

										var data_post = $('form[name=property-frm]').serialize();


										console.log(data_post);

										Property.publish( data_post );

										event.preventDefault();

								});

							});
								

						</script>
						