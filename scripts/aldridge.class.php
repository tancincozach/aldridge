<?php
set_time_limit(0);
session_start();
/**
*
*Author: Tancinco Zach G.
*
*
*/

require_once('RestApi.class.php');


class Aldridge extends httpWorker
{	

	public $collections = array();


	public function __construct(){

       $this->_endPoint    = 'https://open-api.devicewise.com/api';

       if(isset($_SESSION['devicewise_response']['sessionId'])){

       	 $this->sessionId = $_SESSION['devicewise_response']['sessionId'];	
		 $this->session_org_switch('ALDRIDGE');		

       }	  
	}

	public function segregate_things()
	{
		$things_array  = $this->thing_list();   


	 	if(isset($things_array) && count($things_array) > 0)
	 	{

 			$i=0;
		 	foreach($things_array as $parent_key=> $thing_array){
			

						$things_find_result = $this->thing_find($thing_array['key']);


						$tag_thing = (isset($things_find_result['tags']) && count($things_find_result['tags']) > 0 ? implode(',',$things_find_result['tags']):''); 

						if($tag_thing=='pole') {

							$this->collections['thing'][] = $thing_array;
							$this->collections['thing'][$i]['pole_name'] = @$thing_array['name'];
							$this->collections['thing'][$i]['prop_count'] =  count($thing_array['properties']);				

							if(isset($this->collections['thing'][$i]['alarms']));	
							{
									foreach ($this->collections['thing'][$i]['alarms'] as $alarm=>$alarm_values) {

										if(isset($alarm)){

											$result_alarm = $this->alarm_history(@$this->collections['thing'][$i]['key'] ,$alarm);

											if(!empty($result_alarm)){

												$alarm_ctr=0;

												foreach ($result_alarm as $alarm_key=>$current_alarm) {


														if(!empty($current_alarm)){

																$this->collections['thing'][$i]['alarms'] = array($alarm=>$current_alarm);	
															

															
														}

														$alarm_ctr++;
												}

												//$this->collections['thing'][$i]['alarm_count'] = ($alarm_ctr==0  ? 0 : $alarm_ctr++);
											}
										}					
								}
							}

					

							 $i++;
							
						}
					
						unset($things_find_result);				
		 		
		 	}
/*		echo '<pre>';
		print_r($this->collections['thing']);
		echo '<pre>';*/
		/*	unset($things_array);	


			$i=0;

			foreach ($this->collections['temp_thing'] as $p_thing_arr) {
				
				$this->collections['thing'][] = $p_thing_arr;
				$this->collections['thing'][$i]['pole_name'] = @$p_thing_arr['name'];

				if(isset($this->collections['thing'][$i]['alarms']));	
				{
						foreach ($this->collections['thing'][$i]['alarms'] as $alarm=>$alarm_values) {

							if(isset($alarm)){

								$result_alarm = $this->alarm_history(@$this->collections['thing'][$i]['key'] ,$alarm);

								if(!empty($result_alarm)){
		
									foreach ($result_alarm as $alarm_key=>$current_alarm) {

											if(!empty($current_alarm)){

												$this->collections['thing'][$i]['alarms'] = array($alarm=>$current_alarm);
											}
									}
								}
							}					
					}
				}
				
			 $i++;
			}*/

		}	

	
	 } 



	public function gather_things()
	{	

		$this->segregate_things();

		return $this->collections['thing'];
		
	}

	public function gather_alarms()
	{	
		 $this->segregate_things();

		 print_r($this->collections['thing']);

	 	 $alarms =  $this->collections['thing']['alarms'];

	 	 unset($this->collections['thing']);

	 	 return $alarms;	
	}

	public function gather_properties()
	{	

	 	$this->segregate_things();

	  	$properties =  $this->collections['thing']['properties'];

	 	 unset($this->collections['thing']);

	 	 return $properties;

		
	}

	public function login($username,$password)
	{

	    try {


	    	if(!isset($username) || !isset($password)) {

				throw new Exception('Error: Input required fields.');
	    	}

			$this->userName     = $username;
			$this->userPass     = $password;


		    $result = $this->auth_login();

	

	    	if(isset($result['response']['errorMessages'])){			    
	    		

			    throw new Exception(implode(',',$result['response']['errorMessages']));
			    
			} 

  		    $session_info =$this->session_info();

	


		  	if(isset($session_info['userId']))
		  	{
 				$_SESSION['devicewise_username'] = @$session_info['userName'];
 				unset($session_info);
		  	}


			$_SESSION['login'] = true;
			$_SESSION['devicewise_response'] = $result['response']['auth']['params'];
			$_SESSION['uname'] =$this->userName;
			$_SESSION['pass'] = $this->userPass;

			return true;
	    	
	    } catch (Exception $e) {

	    		$_SESSION['error'] = $e->getMessage();
	    }
	   
	    	    
	}

}

?>
