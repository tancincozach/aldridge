<?php
class Settings{

	public function check_redirect_login()
	{
		if(!isset($_SESSION['login']))
		{
			header('Location: login.php');
		}
	}

	public function check_redirect_index()
	{
		if(isset($_SESSION['login']))
		{
			header('Location: index.php');
		}
	}
}
?>