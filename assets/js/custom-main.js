
 var google_map_api = {

 			markerGroups : {
						"yellow": [],
						"red": []
						},

			customIcons : {
						yellow: {
						icon: 'assets/images/exclamation mark yellow.png'
						},
						red: {
						icon: 'assets/images/target-marker.png'
						}

						},
 		   infoWindow : new google.maps.InfoWindow(),

           setMap:function (m) {
                      x = m.getZoom();
                      c = m.getCenter();
                      google.maps.event.trigger(m, 'resize');
                      m.setZoom(x);
                      m.setCenter(c);
                  },

            getAddress: function (latLng) {
                        $('#geo_address').remove();
                        geocoder = new google.maps.Geocoder();  
                        geocoder.geocode( {'latLng': latLng},
                          function(results, status) {
                     
                            if(status == google.maps.GeocoderStatus.OK) {
                              if(results[0]) {                          
                                                            
                                 $("<input type=\"hidden\"  id=\"geo_address\" value=\""+results[0].formatted_address +"\" >").appendTo('body');
                              }
                              else {                           
                                  $("<input type=\"hidden\"  id=\"geo_address\" value=\"No Result\" >").appendTo('body');
                              }
                            }
                            else {
                              $("<input type=\"hidden\"  id=\"geo_address\" value=\""+status+"\" >").appendTo('body');
                            }

                          });
                
                   }
                ,
                xmlParse:function(str){
                	 if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
							        var doc = new ActiveXObject('Microsoft.XMLDOM');
							        doc.loadXML(str);
							        return doc;
							    }

							    if (typeof DOMParser != 'undefined') {
							        return (new DOMParser()).parseFromString(str, 'text/xml');
							    }

							    return createElement('div', null);
                }
                ,

                createMarker:function(point, name, type, map,html ,icon) {

                	 var marker = new google.maps.Marker({
						        map: map,
						        position: point,
						        icon: icon,
						        // shadow: icon.shadow,
						        type: type
						    });
						    if (!google_map_api.markerGroups[type]) google_map_api.markerGroups[type] = [];
						    google_map_api.markerGroups[type].push(marker);

						    google_map_api.bindInfoWindow(marker, map, google_map_api.infoWindow, html);
						    return marker;

                },

                toggleGroup:function (type) {
						    for (var i = 0; i < markerGroups[type].length; i++) {
						        var marker = markerGroups[type][i];
						        if (!marker.getVisible()) {
						            marker.setVisible(true);
						        } else {
						            marker.setVisible(false);
						        }
						    }
				},

				bindInfoWindow:function(marker, map, infoWindow, html){

						    google.maps.event.addListener(marker, 'click', function () {
						        infoWindow.setContent(html);
						        infoWindow.open(map, marker);

						    });

				},

                initGoogleMap:function(json_data ,  map_container){     
                

                      try{


                          google_location_data = json_data;


                          if(google_location_data.loc=='' || $.isEmptyObject(google_location_data.loc)) throw "ERROR: Empty Coordinates for Google Map Api";




                            var map = new google.maps.Map(map_container, {
                              zoom: 3,
                              center: new google.maps.LatLng(google_location_data.loc.lat, google_location_data.loc.lng),
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                            });
                            

                             var marker;

                              $.each(google_location_data,function(i,val){

                                  var full_address =  '';

                                      full_address+='<table class="table table-bordered table-condensed danger">';
                                      full_address+='<thead>';
                                      full_address+='<tr class="info"><th class="text-center">'+google_location_data.name+'</th></tr>';
                                      full_address+='</thead>';

                                  if(!$.isEmptyObject(google_location_data.alarms)){

                                  
                                      full_address+='<tr class="danger">';
                                      full_address+='<td >ALARMS</td>';
                                      full_address+='</tr>';
                                    $.each(google_location_data.alarms,function(alarm_name,alarm_values){

                                      full_address+='<tr >';
                                      full_address+='<td><label>&nbsp;&nbsp;-'+alarm_name+'</label></td>';
                                      full_address+='</tr>';

                                    });
                                    full_address+='</table>';

                                  }

                                  full_address+='<table class="table table-bordered">';
                                  full_address+='<thead>';
                                  full_address+='<tr class="success"><th >ADDRESS</th></tr>';
                                  full_address+='</thead>';

                                   full_address +='<tr><td><label>&nbsp;&nbsp;-'+google_location_data.loc.addr.streetNumber +' '+
                                              google_location_data.loc.addr.street +' '+ 
                                              google_location_data.loc.addr.city  +' '+
                                              google_location_data.loc.addr.state;+'</label></td></tr>';

                                   full_address+='</table>';




									 var xml_marker = '<markers>';

									 	 var icon ='' , type ='';
                                      $.each(google_location_data.alarms,function(alarm_name,alarm_values){


											$.each(alarm_values , function(i,alarm){
											
 										

											if(aldridge.diplay_alarm_option==false){	

													if(	parseInt(alarm.state) > 0){

														icon='assets/images/exclamation mark yellow.png';
		                                              	type='yellow';

		                                              }

		                                     }else{
													  if(parseInt(alarm.state) > 0 ){

		                                                  icon='assets/images/exclamation mark yellow.png';
		                                                  type='yellow';

		                                            }else{
		                                                  icon ='assets/images/target-marker.png';
		                                                  type='red';


		                                            } 

												}
												
											});		
                                  	     
											   xml_marker +='<marker name="'+google_location_data.id+'"  lat="'+google_location_data.loc.lat+'" lng="'+google_location_data.loc.lng+'"  icon="'+icon+'" type="'+type+'" />';
		                                          
                                        });
							
									xml_marker +='</markers>';


							    	var xml = google_map_api.xmlParse(xml_marker);

								    var markers = xml.documentElement.getElementsByTagName("marker");


								    //console.log(markers.length);
								    for (var i = 0; i < markers.length; i++) {


								        var name = markers[i].getAttribute("name");
								        var icon = markers[i].getAttribute("icon");
								        
								        var address = markers[i].getAttribute("address");
								        var type = markers[i].getAttribute("type");

								        var point = new google.maps.LatLng(
								        parseFloat(markers[i].getAttribute("lat")),
								        parseFloat(markers[i].getAttribute("lng")));


								        var html = "<b>" + name + "</b> <br/>" + address;
								        //var icon = customIcons[type] || {};

								       // console.log(full_address);

								        var marker = google_map_api.createMarker(point, name, type, map,full_address ,icon);

								        google_map_api.bindInfoWindow(marker, map, google_map_api.infoWindow, full_address);
								    }
                                  
                              }); 


                      
                                   

                  }catch( err ){

                         alert(err.message||err);

                  }
                       
          }            
  }


	// function to load content with ajax
	

var interval;



var aldridge  = {

		diplay_alarm_option:false,

		latest_record:{},

		alarms_arr :[],

		properties_arr:[],

		auto_load:function( start ){

		

			if(start==false) {
			      this.exec_rest_interval();      

			  }else{

			    clearInterval(interval);                           

			  }

		},

        exec_rest_interval : function( propType ){

              intervalTime = 30000;
              
              var ajax_request_object = this.load_things();

                  interval = setInterval(
                              function()
                               {
                                  ajax_request_object.done(function(){
                                    aldridge.load_things();                                     
                                 });                                      
                                },
                    intervalTime);  

              setTimeout(function() {

                      aldridge.auto_load(true);

                    $('#auto-load-switch').bootstrapToggle('off'); 
                 
                 }, 300000);        
        },  	

		implement_settings:function(){

		         $('.view-alert , .view-property').on('click' , function(event){

    					var thing = $(this).attr('alt').split('_');

    					if(thing.length > 0){

    						if(thing[0]=='prop'){

    							aldridge.load_properties(thing[1]);	       		

    						}else{

								 aldridge.load_alarms(thing[1]);	       							

    						}
    						

    					}

						

	                    event.preventDefault();

	              });

		         $('.view-location').on('click' , function(event){

		         		var thing_name = $(this).attr('alt').split('|');
		         
		       
	           
		         		aldridge.load_location($('#'+thing_name[1]).val());	           

	                    event.preventDefault();

	              });

		         $('#auto-load-switch').change(function() {

		         		aldridge.auto_load($(this).prop('checked'));
				      
			     });

			     $('#alarm-filter').on('change',function(event){



		     			 if($(this).prop('checked')){

	     			 		 aldridge.diplay_alarm_option = true;

		     			 }else{

		     			 	 aldridge.diplay_alarm_option = false;

		     			 }

		     			 aldridge.load_things();

		     			 event.preventDefault();


			     });

		},

		show_modal:function( modal_config ){




							if(! $.isEmptyObject(modal_config)){

								var modal_id = '';

								if(!$.isEmptyObject(modal_config.thing_id) || modal_config.thing_id!=''){
									 modal_id = ' id="'+modal_config.thing_id+'"';
								}

								var modal_html = '<div '+modal_id+' class="modal fade no-display" tabindex="-1" >'+
												'<div class="modal-dialog">'+
													'<div class="modal-content">'+
														'<div class="modal-header">'+
															'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">'+
																'&times;'+
															'</button>'+
															'<h4 class="modal-title">Responsive</h4>'+
														'</div>'+
														'<div class="modal-body">'+
															'<div class="row">'+
																'<div class="col-md-12">'+modal_config.html+'</div>'+															
															'</div>'+
														'</div>'+
														'<div class="modal-footer">'+
															modal_config.button_html
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>';

						       $(modal_html).modal({keyboard:false, backdrop:false})
						            .on('hidden.bs.modal', function (e) {
						                $(this).remove();
						            }); 

							}
							
		},

		load_location:function(thing_location){


			
			try{
					
					if(thing_location=='') throw "Error: Coordinates not available.";

					var location_data = JSON.parse(decodeURIComponent(thing_location));




					this.show_modal({html:'<div id="map-container" style="width:500px;height:480px;"></div>',button_html:'<button type="button" data-dismiss="modal" class="btn btn-light-grey">'+
																'Close'+
															'</button>'});


					$('#map-container').css({height:'480px;',width:$('#map-container').parent().width()+'px'})

				   google_map_api.initGoogleMap(location_data ,  document.getElementById("map-container"));


				   google.maps.event.addDomListener(window, 'load', aldridge.initGoogleMap);


	          }catch( err ){

	                    // alert(err.message||err);

	              }
			



		},

		load_properties:function(thing_id){


			// '<button type="button" data-dismiss="modal" class="btn btn-light-grey">'+
			// 													'Close'+
			// 												'</button>'+
			// 												'<button type="button" class="btn btn-blue">'+
			// 													'Save changes'+
			// 												'</button>'
				
				this.show_modal({html:this.properties_arr[thing_id] ,button_html:'<button type="button" data-dismiss="modal" class="btn btn-light-grey">'+
																'Close'+
															'</button>'});

		},

		load_alarms:function(thing_id){

				this.show_modal({html:this.alarms_arr[thing_id] ,button_html:'<button type="button" data-dismiss="modal" class="btn btn-light-grey">'+
																'Close'+
															'</button>'});

		},

		 load_things :function() {

				 return $.ajax({ 
	                      url:'scripts/ajax.php',                      
	                      type:'POST',
	                      data:{controller:'collect-things'},
	                      beforeSend:function (JsonObj)
	                      {
	                        $('.thing-content').removeClass("fadeIn shake");


									$(".thing-container").append('<div class="ajax-white-backdrop" style="display:block"></div>');

									 $('.ajax-white-backdrop').show();

									if($body.hasClass("sidebar-mobile-open")) {
										var configAnimation, extendOptions, globalOptions = {
											duration: 200,
											easing: "ease",
											mobileHA: true,
											progress: function() {
												activeAnimation = true;
											}
										};
										extendOptions = {
											complete: function() {
												inner.attr('style', '').removeClass("inner-transform");
												$body.removeClass("sidebar-mobile-open");
												activeAnimation = false;
											}
										};
										configAnimation = $.extend({}, extendOptions, globalOptions);

										inner.velocity({
											translateZ: 0,
											translateX: [-sidebarWidth, 0]
										}, configAnimation);
									} 
									$('input[data-toggle=toggle]').bootstrapToggle('disable');
	                      }
	                      ,
	                      success:function( JsonObj ) {                   	

	                       $('.ajax-white-backdrop').hide();      
	                       $('input[data-toggle=toggle]').bootstrapToggle('enable');

	                      	if( $.isEmptyObject(JsonObj.error)){

	                      		aldridge.latest_record  = JsonObj;

	                      		aldridge.thing_template('upper_block','',$('.thing-content'));		                      		

	                      		aldridge.properties		= aldridge.thing_template('property_table');

	                      		aldridge.alarms 		= aldridge.thing_template('alarm_table');
	                      		
	                      		aldridge.implement_settings();

	                      	}else{	     
	                      		aldridge.thing_template('error_message',JsonObj,$('.thing-container'));	
	                      	}
	                     	


	                      },
	                      error:function( errorCode ){
                              $('.ajax-white-backdrop').hide();      
                              $('.thing-container').html(errorCode);
	                      },
	                      dataType: "json"
	                    });
			
		},

		thing_template:function( template,json_data='',container='' ){

			switch(template){
				case 'error_message':

							$html = '<div class="alert alert-danger">'+
								'<button class="close" data-dismiss="alert">'+
									'×'+
								'</button>'+
								'<strong>'+json_data.error+'</strong>'+
							'</div>';

				break;

				case 'upper_block':



				var color   = ['partition-green','partition-blue','partition-red','partition-azure'], alarm_count=0 , properties =0  , $html ='' , total_alarms=0;
				


				$.each(this.latest_record,function(i,json_result){

								

						alarm_count=0;
						properties=0;


						$.each(json_result.alarms,function(i,val_alarm){
						
									$.each(val_alarm , function(alarm_key,alarm_value){


										if(typeof alarm_value === 'object'){

											 state = alarm_value.state;

										}
										else{
											 state = val_alarm.state;											
														
										}

										if(state > 0){
											alarm_count++;
										}


									});	
						});



						$.each(json_result.properties,function(){
							properties++;
						});
/*
						console.log('thing name :'+json_result.name);
						console.log('alarms :'+alarm_count);*/

								if(alarm_count  > 0 ){


								 $html+='<div class="col-md-6 col-lg-3 col-sm-6">'+
			   						 '<div class="panel panel-default panel-white core-box">'+
								   			'<div class="panel-tools">'+
								   			'</div>'+
								   			'<div class="panel-body no-padding">'+		
										   			'<div class="padding-20 text-center core-icon '+color[i]+'">'+
														'<i class="fa  fa-database  fa-2x icon-big "></i>'+
													'</div>'+
												'<div class="core-content">'+
													'<h3 class="title block no-margin padding-5">'+json_result.name+'</h3>'+
													'<span class="subtitle"></span>'+
													'<div class="table-responsive">'+
														'<table class="table padding-20" style="font-size:10px">'+
														'<tr>'+
															'<td>ALARM</td>'+

															'<td class="padding-5">'+
															'<a href="#" data-toggle="tooltip" data-placement="top"  alt="alarm_'+i+'"  class="view-alert  padding-5 text-center text-white tooltips '+color[i]+'" ><i class="glyphicon glyphicon-search"></i></a>'+
																'<span class="label label-danger vertical-align-top " style="margin:5px">'+alarm_count+'</span>'+
															'</td>'+
														'</tr>'+
														'<tr>'+
															'<td>PROPERTY</td>'+
															'<td class="padding-5">'+
															'<a href="#" data-toggle="tooltip" data-placement="top" title="View Property" alt="prop_'+i+'" class="view-property padding-5 text-center text-white tooltips '+color[i]+'" ><i class="glyphicon glyphicon-search"></i></a>'+
															'<span class="label label-success vertical-align-top " style="margin:5px">'+properties+'</span>'+
															'</td>'+
														'</tr>'+
															'<tr>'+
															'<td>LOCATION</td>'+
															'<td class="padding-5">'+
															'<a href="#" data-toggle="tooltip" data-placement="top" alt="'+json_result.name+'|'+json_result.id+'" title="View Property" class="view-location padding-5 text-center text-white tooltips '+color[i]+'" ><i class="glyphicon glyphicon-search"></i></a>'+
															'<input type="hidden"  id="'+json_result.id+'" value="'+encodeURIComponent(JSON.stringify(json_result))+'"/>'+
															'</td>'+
														'</tr>'+
													'</table>'+
													'</div>	'+										
												'</div>'+
											'</div>'+		
										'</div>'+															
									'</div>';
								}	

							total_alarms+=alarm_count;

							});	

					
						if(total_alarms <=0 ){

							aldridge.thing_template('error_message',{error:'Error: No thing found with active alarm(s) on devicewise. '},$('.thing-container'));	
						}


				break;

				case 'property_table':



				
						var $html ='';

							$.each(this.latest_record,function(i,json_result){

							var  inner_html = '';

												$.each(json_result.properties,function(val_key,val_property){

														

													var inner_html2 = '<table class="table table-striped table-hover">';

											

															$.each(val_property , function(prop_key,prop_value){



																		inner_html2 +='<tr>'+
																						  '<td><strong>'+prop_key+':</strong>'+prop_value+'</td>'+
																					  '</tr>';

															});	

													 inner_html2 +='</table>';
													
													inner_html +='<tr>'+
																		'<td><strong>'+val_key+'</strong></td>'+
																		'<td>'+inner_html2+'</td>'+
																 '</tr>';
												});

											
											aldridge.properties_arr.push(
															'<table  id="thing_'+i+'" class="table table-striped table-hover"  >'+
																'<thead>'+
																		'<tr>'+
																			'<th>Property Name</th>'+
																			'<th>Value</th>'+
																		'</tr>'+
																'</thead>'+
																'<tbody>'+
																inner_html+
																'</tbody>'+											
																'</table>'
															);

										});


	
				break;

				case 'alarm_table':

			

					var $html ='';



					$.each(this.latest_record,function(i,json_result){

								var  inner_html = '';

												$.each(json_result.alarms,function(val_key,val_alarm){

														

													var inner_html2 = '<table class="table table-striped table-hover">';



														var state;

															$.each(val_alarm , function(alarm_key,alarm_value){



																	if(typeof alarm_value === 'object'){

																			 state = alarm_value.state;

																			$.each( alarm_value ,function(  key, value ){


																			inner_html2 +='<tr>'+
																							  '<td><strong>'+key+':</strong>'+value+'</td>'+
																						  '</tr>';

																			});		
																	}
																	else{
																		 state = val_alarm.state;

																					inner_html2 +='<tr>'+
																										  '<td><strong>'+alarm_key+':</strong>'+alarm_value+'</td>'+
																									  '</tr>';
																					
																	}
																										
																
															});	

													 inner_html2 +='</table>';

													if(state > 0){

														inner_html +='<tr>'+
																		'<td><strong>'+val_key+'</strong></td>'+
																		'<td>'+inner_html2+'</td>'+
																 '</tr>';

													}
													
												});


												aldridge.alarms_arr.push(

														'<table  class="table table-striped table-hover"  >'+
															'<thead>'+
																	'<tr>'+
																		'<th>Alarm Name</th>'+
																		'<th>Value</th>'+
																	'</tr>'+
															'</thead>'+
															'<tbody>'+
															inner_html+
															'</tbody>'+											
															'</table>'

													);
											});
									
				break;			
		
			}

			if(container) {
				container.html($html);	
			}else{
				return $html;
			}

		},


		init:function(){

			this.load_things();			

		}
}



$(function(){

	aldridge.init();

});