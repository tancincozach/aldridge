 var google_map_api = {

           setMap:function (m) {
                      x = m.getZoom();
                      c = m.getCenter();
                      google.maps.event.trigger(m, 'resize');
                      m.setZoom(x);
                      m.setCenter(c);
                  }
              ,
            getAddress: function (latLng) {
                        $('#geo_address').remove();
                        geocoder = new google.maps.Geocoder();  
                        geocoder.geocode( {'latLng': latLng},
                          function(results, status) {
                     
                            if(status == google.maps.GeocoderStatus.OK) {
                              if(results[0]) {                          
                                                            
                                 $("<input type=\"hidden\"  id=\"geo_address\" value=\""+results[0].formatted_address +"\" >").appendTo('body');
                              }
                              else {                           
                                  $("<input type=\"hidden\"  id=\"geo_address\" value=\"No Result\" >").appendTo('body');
                              }
                            }
                            else {
                              $("<input type=\"hidden\"  id=\"geo_address\" value=\""+status+"\" >").appendTo('body');
                            }

                          });
                
                   }
                ,
                initGoogleMap:function( map_container){     
                  
                   $('#location').remove();

                      try{

                        if($('#json-location').length > 0){

                          google_location_data = JSON.parse($('#json-location').val());

                          

                          if(google_location_data=='' || $.isEmptyObject(google_location_data)) throw "ERROR: Empty Coordinates for Google Map Api";

                            var map = new google.maps.Map(map_container, {
                              zoom: 3,
                              center: new google.maps.LatLng(google_location_data[0].geo_loc.lat, google_location_data[0].geo_loc.lng),
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                            });

                            var infowindow = new google.maps.InfoWindow();




                            if(google_location_data!=''){

                              var marker;
                              $.each(google_location_data,function(i,val){

                                  var full_address =  '';

                                      full_address+='<table class="table table-bordered table-condensed danger">';
                                      full_address+='<thead>';
                                      full_address+='<tr class="info"><th class="text-center">'+val.name+'</th></tr>';
                                      full_address+='</thead>';

                                  if(!$.isEmptyObject(val.alarms)){

                                  
                                      full_address+='<tr class="danger">';
                                      full_address+='<td >ALARMS</td>';
                                      full_address+='</tr>';
                                    $.each(val.alarms,function(alarm_name,alarm_values){

                                      full_address+='<tr >';
                                      full_address+='<td><label>&nbsp;&nbsp;-'+alarm_name+'</label></td>';
                                      full_address+='</tr>';

                                    });
                                    full_address+='</table>';

                                  }

                                  full_address+='<table class="table table-bordered">';
                                  full_address+='<thead>';
                                  full_address+='<tr class="success"><th >ADDRESS</th></tr>';
                                  full_address+='</thead>';

                                   full_address +='<tr><td><label>&nbsp;&nbsp;-'+val.geo_loc.addr.streetNumber +' '+
                                              val.geo_loc.addr.street +' '+ 
                                              val.geo_loc.addr.city  +' '+
                                              val.geo_loc.addr.state;+'</label></td></tr>';

                                   full_address+='</table>';

                              



                                      if(!$.isEmptyObject(val.alarms)){


                                      $.each(val.alarms,function(alarm_name,alarm_values){

                                          console.log(alarm_values);        
                                          console.log(alarm_values[0].state);

                                            if(alarm_values[0].state==1){
                                                marker = new google.maps.Marker({
                                                    position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
                                                      icon:new google.maps.MarkerImage("assets/images/exclamation mark yellow.png",
                                                                  new google.maps.Size(32, 32),
                                                                      new google.maps.Point(0,0),
                                                                      new google.maps.Point(16, 16)),
                                                    map: map
                                                  });
                                            }

                                            else{

                                               marker = new google.maps.Marker({
                                                        position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
                                                          icon:new google.maps.MarkerImage("assets/images/target-marker.png",
                                                                      new google.maps.Size(32, 32),
                                                                          new google.maps.Point(0,0),
                                                                          new google.maps.Point(16, 16)),
                                                        map: map
                                                      });


                                            } 
                                        });

                                                  
                                      }else{

                                             marker = new google.maps.Marker({
                                                        position: new google.maps.LatLng(val.geo_loc.lat, val.geo_loc.lng),
                                                          icon:new google.maps.MarkerImage("assets/images/target-marker.png",
                                                                      new google.maps.Size(32, 32),
                                                                          new google.maps.Point(0,0),
                                                                          new google.maps.Point(16, 16)),
                                                        map: map
                                                      });

                                      }



                                      google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                        return function() {
                                        infowindow.setContent(full_address);
                                        infowindow.open(map, marker);
                                        }
                                      })(marker, i));
                                  
                              }); 
                            }

                        }                                      

                  }catch( err ){

                         alert(err.message||err);

                  }
                       
          }            
  }


var aldridge  ={

      Init:function(){

               google_map_api.initGoogleMap(document.getElementById("google-map-1"));    
          

              $('.view-alert, .view-property').click(function(event){

                    var table_id = $(this).attr('alt');

                    var $table = $('#'+table_id).clone();


                    bootbox.alert($table.html());

                    event.preventDefault();

              });

              
                interval = setInterval(

                                function()
                                {

                                  ajaxLoaderWithOutOverlay('things.php', $("#ajax-content"));

                                }   
                                                                   
                              ,60000);  

      },

      Login:function( username,password){
               try
              {
                if(username=='' ||  password=='') throw "ERROR: Please Input Username/Password";

                 $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'checkLogin',uname:username,pass:password},
                      dataType: "json",
                      beforeSend:function (JsonObj)
                      {
                        $('.msg').html("<img src='images/icons/loading.gif' width='20'>&nbsp; Please wait while the system is checking your credentials.");
                      }
                      ,
                      success:function( JsonObj ) {                         
                      

                        if(!$.isEmptyObject(JsonObj.error))
                        {                            
                            $('.msg').html('<span class="glyphicon glyphicon glyphicon-minus-sign " style="color:red"></span>&nbsp;<strong>ERROR :'+JsonObj.error+'</strong> ');    
                        }
                        else
                        {
                          $('.msg').html('Login Successful. Redirecting....');
                             setTimeout(function() {

                                    window.location='index.php';
                                                                   
                               }, 4000); 

                             setTimeout(function() {

                                      window.location='index.php';
                                                                   
                               }, 2000); 

                        }

                      },
                      error:function( JsonObj ){
                                  $('.msg').html('<span class="glyphicon glyphicon glyphicon-minus-sign " style="color:red"></span>&nbsp;<strong>ERROR : Unable to process login</strong> ');    
                      }
                    });
              }
               catch( err )
              {
                     alert(err.message||err);
              }
        }
}


